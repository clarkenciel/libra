# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'libra/version'

Gem::Specification.new do |spec|
  spec.name          = 'libra'
  spec.version       = Libra::VERSION
  spec.authors       = %w(danny clarke)
  spec.email         = %w(clarkenciel@gmail.com)

  spec.homepage      = 'https://gitlab.com/clarkenciel/libra'
  spec.summary       = 'flexible policy-based authorization'
  spec.description   = <<-STRING
  Libra tries to provide an unintrusive and flexible system to create
  policy-based authorization.

  Libra's core evalution classes validate the ability of
  "actors" (probably users) to operate on
  "resources" (anything else in your system) given configurable
  "requirements" (predicates defined against actors and resources).

  This gem provides the evaluation classes and a set of mixins to
  facilitate the creation of a libra-compatible system.
  STRING

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = %w(lib)
  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the
  # 'allowed_push_host' to allow pushing to a single host or delete this
  # section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.add_development_dependency 'activesupport', '~> 5.1.4'
  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.52.1'
end
