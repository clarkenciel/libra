# frozen_string_literal: true

RSpec.describe Libra do
  it 'has a version number' do
    expect(Libra::VERSION).not_to be nil
  end
end
