# Libra

Libra tries to provide an unintrusive and flexible system to create
policy-based authorization.

Libra's core evalution classes validate the ability of "actors" (probably users) to operate on
"resources" (anything else in your system) given configurable
"requirements" (predicates defined against actors and resources).

This gem provides the evaluation classes and a set of mixins to
facilitate the creation of a libra-compatible system.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'libra', git: 'https://gitlab.com/clarkenciel/libra'
```

And then execute:

    $ bundle

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/clarkenciel/libra. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Code of Conduct

Everyone interacting in the Libra project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/clarkenciel/libra/blob/master/CODE_OF_CONDUCT.md).
